import React, { useContext, useReducer } from 'react';
import './App.css';
import StyledComponentSample from '../StyledComponentSample';
import AppLayout from './AppLayout';
import AppBar from './AppBar';
import AppContext from './AppContext';
import AppReducer from './AppReducer';

const App = () => {

  /**
   1. 先定義state初始的值 -> AppContext
   2. 再定義遇到什麼action時要對state做什麼操作 -> AppReducer
   3. 利用useContext, 把第一步定義的值轉化為state -> initial = useContext(AppContext)
   4. 利用useReducer, 製造出對這個context用什麼reducer的dispatch及取值用的state
   5. 把需要用到這些的component用AppContext.Provider包住, 將state, dispatch往下傳
  */

  const initialState = useContext(AppContext);
  const [appState, appDispatch] = useReducer(AppReducer, initialState);

  return (
    <AppContext.Provider value={{appState, appDispatch}}>
      <AppLayout>
        <AppBar />
        <h3>App</h3>
        <hr size="8px" align="center" width="100%" />
        <StyledComponentSample />
        <hr size="8px" align="center" width="100%" />
      </AppLayout>
    </AppContext.Provider>
  );
}

export default App;
