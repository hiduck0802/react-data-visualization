import React from 'react';
import styled, { css } from 'styled-components';

const BaseButton = styled.button`
  background-color: grey;
  width: 80px;
  height: 30px;
  ${props => props.info && css`
    background-color: limegreen;
  `}
  ${props => props.warning && css`
    background-color: yellow;
  `}
`;

export default () => {
    return (
        <div>
            <div>StyledComponentSample</div>
            <BaseButton>Block</BaseButton> 
            <BaseButton info>Info</BaseButton>
            <BaseButton warning>Warning</BaseButton>
        </div>
    )
}